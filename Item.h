
#ifndef ITEM_H
#define ITEM_H

#include <iostream>
#include <string>
using namespace std;
/**
 * @brief The Item class player can equip many items this is a parent class of many subclasses
 */
class Item
{
public:
    /**
 * @brief getValue How much the item cost
 * @return
 */
int getValue();
/**
 * @brief toString virtual function that returns a basic output, subclasses are more specific
 * @return
 */
virtual string toString();
/**
 * @brief getName use by class and subclasses to return the name of the item
 * @return as a string
 */
string getName();
/**
 * @brief Item can be equiped by the user
 * @param ItemName what we are going to call the item
 * @param cost how much does the item cost
 */
Item(string ItemName,int cost);

private:
string name;
int value;
};



#endif // ITEM_H
