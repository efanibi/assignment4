#ifndef WEAPON_H
#define WEAPON_H
#include "Item.h"
class  Weapon:public Item
{
private:
int damage;
int range;
/**
 * @brief The Weapon Class is for basic weapons
 */
public:
Weapon(string ItemName,int damageValue,int cost);
/**
 * @brief toString
 * @return a string with data about short range weapons
 */
string toString();
/**
 * @brief getDamage
 * @return how much damage a weapon does
 */
int getDamage();
/**
 * @brief getRange
 * @return the range of a weapon, usually zero for the weappon class
 */
virtual int getRange();
/**
 * @brief use
 * @param rangeToTarget how far the target is
 * @return the amont of damgage the weapon does
 */
virtual int use(int rangeToTarget);
};



#endif // WEAPON_H
