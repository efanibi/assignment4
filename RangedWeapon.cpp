#include "RangedWeapon.h"
RangedWeapon::RangedWeapon(string ItemName,int damageValue,int rangeValue,int ammoValue,int cost)
    :Weapon(ItemName,damageValue,cost)
{
ammo=ammoValue;
range=rangeValue;
}

string RangedWeapon::toString()
{
    string mystring="";
    string valuestring=to_string(this->getValue());
    string Damagestring=to_string(this->getDamage());
    string Ammostring=to_string(this->getAmmo());
    string Rangestring=to_string(this->getRange());
    mystring=mystring+this->getName()+' '+"(value: "+valuestring+')'+" Damage: "+Damagestring+" Ammo: "+Ammostring+" Range: "+Rangestring;;
    return mystring;
}
int RangedWeapon::getRange()
{
return range;
}
int RangedWeapon::getAmmo()
{
return ammo;
}
int RangedWeapon::use(int rangeToTarget)
{
int myammo=this->getAmmo();
int weaponrange=this->getRange();
int mydamage=this->getDamage();
int hurt=0;
if(myammo==0)
{
return hurt;
}

if(rangeToTarget>weaponrange)
{
    hurt=0;
    ammo=ammo-1;
 }
if(rangeToTarget<=weaponrange)
   {
    hurt=mydamage;
    ammo=ammo-1;
    }
return hurt;
}



